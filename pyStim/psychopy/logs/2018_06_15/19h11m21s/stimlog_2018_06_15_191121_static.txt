Fri, 15 Jun 2018 19:11:21
1 rep(s) of 2 stim(s) generated. 
387/18000 frames displayed. Average fps: 180.16 hz.
Elapsed time: 2.148 seconds.

Stim #0 (static):
   alpha: 1
   blend_jumps: False
   check_size: [50, 50]
   check_type: board
   color: [-1, -1, -1]
   color_mode: rgb
   contrast_channel: all
   contrast_opp: opposite
   delay: 0
   duration: 100
   end_delay: 0
   fill_mode: uniform
   fill_seed: 1
   force_stop: 0
   image_channel: all
   image_filename: None
   image_size: [100, 100]
   inner_diameter: 50
   intensity: 1
   intensity_dir: both
   location: [0, 0]
   move_delay: 0
   move_seed: 1
   movie_filename: None
   movie_size: [100, 100]
   num_check: 64
   num_dirs: 1
   num_jumps: 5
   ori_with_dir: False
   orientation: 0
   outer_diameter: 100
   period_mod: 1
   phase: [0, 0]
   phase_speed: [0, 0]
   sf: 1
   shape: rectangle
   shuffle: False
   size: [608, 684]
   speed: 300
   start_dir: 90
   start_radius: 100
   table_filename: None
   table_type: polar
   timing: step
   travel_distance: 50
   trigger: False


Stim #1 (static):
   alpha: 1
   blend_jumps: False
   check_size: [50, 50]
   check_type: board
   color: [-1, 1, -1]
   color_mode: intensity
   contrast_channel: all
   contrast_opp: opposite
   delay: 0
   duration: 100
   end_delay: 0
   fill_mode: uniform
   fill_seed: 1
   force_stop: 0
   image_channel: all
   image_filename: None
   image_size: [100, 100]
   inner_diameter: 50
   intensity: 1
   intensity_dir: both
   location: [-200, -200]
   move_delay: 0
   move_seed: 1
   movie_filename: None
   movie_size: [100, 100]
   num_check: 64
   num_dirs: 1
   num_jumps: 5
   ori_with_dir: False
   orientation: 0
   outer_diameter: 100
   period_mod: 1
   phase: [0, 0]
   phase_speed: [0, 0]
   sf: 1
   shape: rectangle
   shuffle: False
   size: [50, 100]
   speed: 300
   start_dir: 90
   start_radius: 100
   table_filename: None
   table_type: polar
   timing: step
   travel_distance: 50
   trigger: False




#BEGIN PICKLE#
�]q (}q(X   shapeqX	   rectangleqX   orientationqK X   locationq]q(K K eX   sizeq]q(M`M�eX   inner_diameterq	K2X   outer_diameterq
KdX   delayqK X   durationqKdX	   end_delayqK X
   force_stopqK X   triggerq�X
   color_modeqX   rgbqX   colorq]q(J����J����J����eX   contrast_channelqX   allqX	   intensityqKX   timingqX   stepqX	   fill_modeqX   uniformqX   alphaqKX   intensity_dirqX   bothqX   contrast_oppqX   oppositeqX
   check_typeq X   boardq!X   sfq"KX   phaseq#]q$(K K eX   phase_speedq%]q&(K K eX   image_channelq'X   allq(X	   fill_seedq)KX
   check_sizeq*]q+(K2K2eX	   num_checkq,K@X   image_filenameq-NX
   image_sizeq.]q/(KdKdeX   movie_filenameq0NX
   movie_sizeq1]q2(KdKdeX
   period_modq3KX   speedq4M,X	   start_dirq5KZX   num_dirsq6KX   start_radiusq7KdX
   move_delayq8K X   ori_with_dirq9�X   travel_distanceq:K2X	   move_seedq;KX   table_filenameq<NX
   table_typeq=X   polarq>X	   num_jumpsq?KX   shuffleq@�X   blend_jumpsqA�X	   move_typeqBX   staticqCu}qD(hX	   rectangleqEhK h]qF(J8���J8���eh]qG(K2Kdeh	K2h
KdhK hKdhK hK h�hX	   intensityqHh]qI(J����KJ����ehX   allqJhKhX   stepqKhX   uniformqLhKhX   bothqMhX   oppositeqNh X   boardqOh"Kh#]qP(K K eh%]qQ(K K eh'X   allqRh)Kh*]qS(K2K2eh,K@h-Nh.]qT(KdKdeh0Nh1]qU(KdKdeh3Kh4M,h5KZh6Kh7Kdh8K h9�h:K2h;Kh<Nh=X   polarqVh?Kh@�hA�hBX   staticqWue.