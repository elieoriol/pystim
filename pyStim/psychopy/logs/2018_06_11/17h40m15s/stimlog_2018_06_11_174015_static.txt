Mon, 11 Jun 2018 17:40:15
1 rep(s) of 2 stim(s) generated. 
2700/2700 frames displayed. Average fps: 180.53 hz.
Elapsed time: 14.956 seconds.

Stim #0 (static):
   alpha: 1
   blend_jumps: False
   check_size: [50, 50]
   check_type: board
   color: [-1, -1, -1]
   color_mode: rgb
   contrast_channel: all
   contrast_opp: opposite
   delay: 0
   duration: 15
   end_delay: 0
   fill_mode: uniform
   fill_seed: 1
   force_stop: 0
   image_channel: all
   image_filename: None
   image_size: [100, 100]
   inner_diameter: 50
   intensity: 1
   intensity_dir: both
   location: [0, 0]
   move_delay: 0
   move_seed: 1
   movie_filename: None
   movie_size: [100, 100]
   num_check: 64
   num_dirs: 1
   num_jumps: 5
   ori_with_dir: False
   orientation: 0
   outer_diameter: 100
   period_mod: 1
   phase: [0, 0]
   phase_speed: [0, 0]
   sf: 1
   shape: rectangle
   shuffle: False
   size: [600, 600]
   speed: 300
   start_dir: 90
   start_radius: 100
   table_filename: None
   table_type: polar
   timing: step
   travel_distance: 50
   trigger: False


Stim #1 (moving):
   alpha: 1
   blend_jumps: False
   check_size: [50, 50]
   check_type: board
   color: [-1, 1, -1]
   color_mode: rgb
   contrast_channel: all
   contrast_opp: opposite
   delay: 1
   duration: 5
   end_delay: 0
   fill_mode: uniform
   fill_seed: 1
   force_stop: 0
   image_channel: all
   image_filename: None
   image_size: [100, 100]
   inner_diameter: 50
   intensity: 1
   intensity_dir: both
   location: [0, 0]
   move_delay: 5
   move_seed: 1
   movie_filename: None
   movie_size: [100, 100]
   num_check: 64
   num_dirs: 1
   num_jumps: 5
   ori_with_dir: False
   orientation: 0
   outer_diameter: 100
   period_mod: 1
   phase: [0, 0]
   phase_speed: [0, 0]
   sf: 1
   shape: circle
   shuffle: False
   size: [600, 600]
   speed: 100
   start_dir: 90
   start_radius: 100
   table_filename: None
   table_type: polar
   timing: step
   travel_distance: 50
   trigger: False




#BEGIN PICKLE#
�]q (}q(X   shapeqX	   rectangleqX   orientationqK X   locationq]q(K K eX   sizeq]q(MXMXeX   inner_diameterq	K2X   outer_diameterq
KdX   delayqK X   durationqKX	   end_delayqK X
   force_stopqK X   triggerq�X
   color_modeqX   rgbqX   colorq]q(J����J����J����eX   contrast_channelqX   allqX	   intensityqKX   timingqX   stepqX	   fill_modeqX   uniformqX   alphaqKX   intensity_dirqX   bothqX   contrast_oppqX   oppositeqX
   check_typeq X   boardq!X   sfq"KX   phaseq#]q$(K K eX   phase_speedq%]q&(K K eX   image_channelq'X   allq(X	   fill_seedq)KX
   check_sizeq*]q+(K2K2eX	   num_checkq,K@X   image_filenameq-NX
   image_sizeq.]q/(KdKdeX   movie_filenameq0NX
   movie_sizeq1]q2(KdKdeX
   period_modq3KX   speedq4M,X	   start_dirq5KZX   num_dirsq6KX   start_radiusq7KdX
   move_delayq8K X   ori_with_dirq9�X   travel_distanceq:K2X	   move_seedq;KX   table_filenameq<NX
   table_typeq=X   polarq>X	   num_jumpsq?KX   shuffleq@�X   blend_jumpsqA�X	   move_typeqBX   staticqCu}qD(hX   circleqEhK h]qF(K K eh]qG(MXMXeh	K2h
KdhKhKhK hK h�hhh]qH(J����KJ����ehhhKhhhhhKhhhhh h!h"Kh#]qI(K K eh%]qJ(K K eh'h(h)Kh*]qK(K2K2eh,K@h-Nh.]qL(KdKdeh0Nh1]qM(KdKdeh3Kh4Kdh5KZh6Kh7Kdh8Kh9�h:K2h;Kh<Nh=h>h?Kh@�hA�hBX   movingqNue.